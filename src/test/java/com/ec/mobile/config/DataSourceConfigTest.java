package com.ec.mobile.config;

import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.Test;
import static org.junit.Assert.*;
import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=com.ec.mobile.config.DataSourceConfig.class)
public class DataSourceConfigTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcOperations jdbcOperations;

    @Test
    public void dataSourceShouldNotBeNull() {
    	assertNotNull(dataSource);
    }
    
    @Test
    public void mobileJdbcOperationsShouldNotBeNull() {
    	assertNotNull(jdbcOperations);
    }
}

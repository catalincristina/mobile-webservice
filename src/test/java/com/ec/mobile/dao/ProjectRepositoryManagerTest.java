package com.ec.mobile.dao;

import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.junit.Test;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=com.ec.mobile.config.DataSourceConfig.class)
public class ProjectRepositoryManagerTest {

	@Autowired
    private JdbcOperations jdbcOperations;

    @Test
    public void jdbcOperationsShouldNotBeNull() {
    	assertNotNull(jdbcOperations);
    }
}

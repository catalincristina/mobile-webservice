package com.ec.mobile.dto;

import java.sql.Timestamp;
import com.ec.mobile.constant.SessionConstants;

public final class Session {

    private long id;
    private final long userId;
    private final String username;
    private final String email;
    private String token;
    private final Timestamp startTime;
    private final Timestamp endTime;
    private final boolean status;

    public Session(User user) {
	userId = user.getId();
	username = user.getUsername();
	email = user.getEmail();
	startTime = new Timestamp(System.currentTimeMillis());
	endTime = new Timestamp(startTime.getTime() + SessionConstants.SESSION_TIME);
	status = true;
    }

    public Session(long id, long userId, String username, String token, Timestamp startTime, Timestamp endTime, boolean status, String email) {
	this.id = id;
	this.userId = userId;
	this.username = username;
	this.email = email;
	this.token = token;
	this.startTime = startTime;
	this.endTime = endTime;
	this.status = status;
    }

    public long getId() {
	return id;
    }

    public long getUserId() {
	return userId;
    }

    public String getUsername() {
	return username;
    }

    public String getEmail() {
	return email;
    }

    public String getToken() {
	return token;
    }

    public Timestamp getStartTime() {
	return new Timestamp(startTime.getTime());
    }

    public Timestamp getEndTime() {
	return new Timestamp(endTime.getTime());
    }

    public boolean getStatus() {
	return status;
    }
    
    public void setToken(String token) {
	if (token == null) { throw new NullPointerException(); }
	if (this.token == null) { this.token = token; }
    }

    public boolean isValid() {
	Timestamp currentTime = new Timestamp(System.currentTimeMillis());
	return startTime.getTime() < currentTime.getTime() && currentTime.getTime() < endTime.getTime() ? true : false;
    }
}

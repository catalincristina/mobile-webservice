package com.ec.mobile.dto;

import java.sql.Date;

public final class Record {

	private final long id;
	private final String taskName;
	private final long processId;
	private final long referenceId;
	private final String referenceTable;
	private final Date requestDate;
	private final String store;
	private final String requesterName;
	private final double advance;
	private final double deduction;
	private final String expense;
	private final String currency;

	public Record(long id, String taskName, long processId, long referenceId, String referenceTable, Date requestDate,
			String store, String requesterName, double advance, double deduction, String expense, String currency) {
		this.id = id;
		this.taskName = taskName;
		this.processId = processId;
		this.referenceId = referenceId;
		this.referenceTable = referenceTable;
		this.requestDate = requestDate;
		this.store = store;
		this.requesterName = requesterName;
		this.advance = advance;
		this.deduction = deduction;
		this.expense = expense;
		this.currency = currency;
	}
	
	public long getId() {
		return id;
	}

	public String getTaskName() {
		return taskName;
	}

	public long getProcessId() {
		return processId;
	}

	public long getReferenceId() {
		return referenceId;
	}

	public String getReferenceTable() {
		return referenceTable;
	}

	public Date getRequestDate() {
		return new Date(requestDate.getTime());
	}

	public String getStore() {
		return store;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public double getAdvance() {
		return advance;
	}

	public double getDeduction() {
		return deduction;
	}

	public String getExpense() {
		return expense;
	}

	public String getCurrency() {
		return currency;
	}
}

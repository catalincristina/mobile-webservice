package com.ec.mobile.dto;

public class Payload {

	private final long id;
	private final int projectId;
	private final long processId;
	private final long categoryId;
	private final String username;
	private final boolean push;

	public Payload(long id, long processId, long categoryId, String email, boolean push) {
		this.id = id;
		projectId = 1;
		this.processId = processId;
		this.categoryId = categoryId;
		username = email.split("@")[0];
		this.push = push;
	}
	
	public long getId() {
		return id;
	}

	public int getProjectId() {
		return projectId;
	}
	
	public long getProcessId() {
		return processId;
	}
	
	public long getCategoryId() {
		return categoryId;
	}

	public String getUsername() {
		return username;
	}
	
	public boolean getPush() {
		return push;
	}
}

package com.ec.mobile.dto;

import java.util.List;

public final class Section {

	private final String sectionTitle;
	private final List<Field> fields;

	private Section(Builder builder) {
		sectionTitle = builder.sectionTitle;
		fields = builder.fields;
	}

	public String getSectionTitle() {
		return sectionTitle;
	}

	public List<Field> getFields() {
		return fields;
	}

	public static class Builder {

		private final String sectionTitle;
		private List<Field> fields = null;

		public Builder(String sectionTitle) {
			this.sectionTitle = sectionTitle;
		}

		public Builder fields(List<Field> fields) {
			this.fields = fields;
			return this;
		}

		public Section build() {
			return new Section(this);
		}
	}
}

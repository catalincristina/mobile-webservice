package com.ec.mobile.dto;

public final class Project {

    private final int id;
    private final String title;
    private final String icon;
    private final int itemCount;

    public Project(int id, String title, String icon, int itemCount) {
	this.id = id;
	this.title = title;
	this.icon = icon;
	this.itemCount = itemCount;
    }

    public int getId() {
	return id;
    }

    public String getTitle() {
	return title;
    }

    public String getIcon() {
	return icon;
    }

    public int getItemCount() {
	return itemCount;
    }
}

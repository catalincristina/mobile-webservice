package com.ec.mobile.dto;

public class Decision {

	private long id;
	private long processId;
	private long referenceId;
	private String referenceTable;
	private boolean response;
	private String message;
	
	public long getId() {
		return id;
	}

	public long getProcessId() {
		return processId;
	}

	public long getReferenceId() {
		return referenceId;
	}

	public String getReferenceTable() {
		return referenceTable;
	}

	public boolean getResponse() {
		return response;
	}

	public String getMessage() {
		return message;
	}
}

package com.ec.mobile.dto;

public class Credential {

    private String username;
    private String password;

    public String getUsername() {
	return username;
    }

    public String getPassword() {
	return password;
    }
}

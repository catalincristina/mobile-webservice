package com.ec.mobile.dto;

public final class User {

    private final long id;
    private final String username;
    private final String email;

    public User(Long id, String username, String email) {
	this.id = id;
	this.username = username;
	this.email = email;
    }

    public long getId() {
	return id;
    }

    public String getUsername() {
	return username;
    }

    public String getEmail() {
	return email;
    }
}

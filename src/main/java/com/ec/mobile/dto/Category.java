package com.ec.mobile.dto;

public final class Category {

    private final long id;
    private final int projectId;
    private final String title;
    private final int itemCount;

    public Category(long id, int projectId, String title, int itemCount) {
	this.id = id;
	this.projectId = projectId;
	this.title = title;
	this.itemCount = itemCount;
    }

    public long getId() {
	return id;
    }

    public int getProjectId() {
	return projectId;
    }

    public String getTitle() {
	return title;
    }

    public int getItemCount() {
	return itemCount;
    }
}

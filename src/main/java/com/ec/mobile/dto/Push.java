package com.ec.mobile.dto;

public class Push {

	private final long processId;
	private final boolean status;
	
	public Push(long processId) {
		this.processId = processId;
		this.status = true;
	}
	
	public long getProcessId() {
		return processId;
	}
	
	public boolean getStatus() {
		return status;
	}
}

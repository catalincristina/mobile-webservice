package com.ec.mobile.dto;

public final class Transport {

	private final long id;
	private final long processId;
	private final long referenceId;
	private final String referenceTable;
	private final Boolean response = null;
	private final String message = null;

	public Transport(long id, long processId, long referenceId, String referenceTable) {
		this.id = id;
		this.referenceId = referenceId;
		this.processId = processId;
		this.referenceTable = referenceTable;
	}
	
	public long getId() {
		return id;
	}

	public long getReferenceId() {
		return referenceId;
	}

	public long getProcessId() {
		return processId;
	}

	public String getReferenceTable() {
		return referenceTable;
	}

	public Boolean getResponse() {
		return response;
	}

	public String getMessage() {
		return message;
	}
}

package com.ec.mobile.dto;

import java.util.List;

public final class Task {

	private final String sectionTitle;
	private final List<Field> fields;
	private final List<Section> details;
	private final Transport decision;

	private Task(Builder builder) {
		sectionTitle = builder.sectionTitle;
		fields = builder.fields;
		details = builder.details;
		decision = builder.decision;
	}

	public String getSectionTitle() {
		return sectionTitle;
	}

	public List<Field> getFields() {
		return fields;
	}

	public List<Section> getDetails() {
		return details;
	}

	public Transport getDecision() {
		return decision;
	}

	public static class Builder {

		private final String sectionTitle;
		private List<Field> fields = null;
		private List<Section> details = null;
		private final Transport decision;

		public Builder(String sectionTitle, Transport decision) {
			this.sectionTitle = sectionTitle;
			this.decision = decision;
		}

		public Builder fields(List<Field> fields) {
			this.fields = fields;
			return this;
		}

		public Builder details(List<Section> details) {
			this.details = details;
			return this;
		}

		public Task build() {
			return new Task(this);
		}
	}
}

package com.ec.mobile.dto;

public final class Detail {

    private final long taskId;
    private final String sectionName;
    private final String key;
    private final String value;

    public Detail(long taskId, String sectionName, String key, String value) {
	this.taskId = taskId;
	this.sectionName = sectionName;
	this.key = key;
	this.value = value;
    }

    public long getTaskId() {
	return taskId;
    }

    public String getSectionName() {
	return sectionName;
    }

    public String getKey() {
	return key;
    }

    public String getValue() {
	return value;
    }
}

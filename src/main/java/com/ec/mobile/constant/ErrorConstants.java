package com.ec.mobile.constant;

public class ErrorConstants {

    public static final String UNAUTHORIZED = "Unauthorized";
    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
    public static final String FORBIDDEN = "Forbidden";
    public static final String BAD_REQUEST = "Bad request";
    public static final String DEFAULT_LANG = "en";
}

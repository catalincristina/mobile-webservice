package com.ec.mobile.constant;

public class SessionConstants {

    public static final String SESSION_TABLE = "web_service";
    public static final Long SESSION_TIME = 72_000_000L;
}

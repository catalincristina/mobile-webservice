package com.ec.mobile.constant;

public class LdapConstants {
    public static final String DEFAULT_INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    public static final String DEFAULT_PROVIDER_URL = "ldap://EUIDC0001:389/OU=ROMANIA,OU=COUNTRIES,dc=dg,dc=carrefour,dc=com";
    public static final String DEFAULT_DOMAIN = "dg.carrefour.com";
}

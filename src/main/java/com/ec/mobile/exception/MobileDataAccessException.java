package com.ec.mobile.exception;

import java.util.Date;

public class MobileDataAccessException extends RuntimeException implements Mailable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String timestamp;
	private final int status;
	private final String error;
	private final String exception;
	private final String message;
	private final String lang;

	private MobileDataAccessException(Builder builder) {
		timestamp = builder.timestamp;
		status = builder.status;
		error = builder.error;
		exception = builder.exception;
		message = builder.message;
		lang = builder.lang;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public int getStatus() {
		return status;
	}

	public String getError() {
		return error;
	}

	public String getException() {
		return exception;
	}

	public String getMessage() {
		return message;
	}
	
	public String getLang() {
		return lang;
	}
	
	public static class Builder {
		
		private final String timestamp;
		private final int status;
		private final String error;
		private final String exception;
		private final String message;
		private String lang = null;
		
		public Builder(int status, String error, String message) {
			timestamp = new Date().toString();
			this.status = status;
			this.error = error;
			exception = this.getClass().getCanonicalName();
			this.message = message;
		}
		
		public Builder setLang(String lang) {
			this.lang = lang;
			return this;
		}
		
		public MobileDataAccessException build() {
			return new MobileDataAccessException(this);
		}
	}
}

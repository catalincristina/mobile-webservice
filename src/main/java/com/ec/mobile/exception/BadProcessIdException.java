package com.ec.mobile.exception;

@SuppressWarnings("serial")
public class BadProcessIdException extends RuntimeException {

    private final String message;

    public BadProcessIdException(String message) {
	this.message = message;
    }

    public String getMessage() {
	return message;
    }
}

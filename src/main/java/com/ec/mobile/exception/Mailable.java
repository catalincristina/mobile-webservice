package com.ec.mobile.exception;

public interface Mailable {

    String getTimestamp();
    int getStatus();
    String getError();
    String getException();
    String getMessage();
    String getLang();
}

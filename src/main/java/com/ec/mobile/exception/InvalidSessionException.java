package com.ec.mobile.exception;

@SuppressWarnings("serial")
public class InvalidSessionException extends RuntimeException {

    private final String message;

    public InvalidSessionException(String message) {
	this.message = message;
    }

    public String getMessage() {
	return message;
    }
}

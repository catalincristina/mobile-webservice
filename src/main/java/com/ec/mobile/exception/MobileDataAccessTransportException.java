package com.ec.mobile.exception;

public class MobileDataAccessTransportException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int status;
	private final String error;
	private final String message;
	
	public MobileDataAccessTransportException(int status, String error, String message) {
		this.status = status;
		this.error = error;
		this.message = message;
	}
	
	public int getStatus() {
		return status;
	}
	
	public String getError() {
		return error;
	}
	
	public String getMessage() {
		return message;
	}
}

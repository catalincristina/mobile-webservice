package com.ec.mobile.exception;

@SuppressWarnings("serial")
public class CredentialNotFoundException extends RuntimeException {

    private final String error;
    private final String lang;

    public CredentialNotFoundException(String error, String lang) {
	this.error = error;
	this.lang = lang;
    }

    public String getError() {
	return error;
    }

    public String getLang() {
	return lang;
    }
}

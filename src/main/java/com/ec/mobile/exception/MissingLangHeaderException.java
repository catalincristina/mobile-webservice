package com.ec.mobile.exception;

@SuppressWarnings("serial")
public class MissingLangHeaderException extends RuntimeException {

    private final String message;

    public MissingLangHeaderException(String message) {
	this.message = message;
    }

    public String getMessage() {
	return message;
    }
}

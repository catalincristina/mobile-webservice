package com.ec.mobile.exception;

import java.util.Date;

@SuppressWarnings("serial")
public class CategoryNotFoundException extends RuntimeException implements Mailable {

    private final String timestamp;
    private final int status;
    private final String error;
    private final String exception;
    private final String message;
    private final String lang;

    public CategoryNotFoundException(int status, String error, String message, String lang) {
	timestamp = new Date().toString();
	this.status = status;
	this.error = error;
	exception = this.getClass().getCanonicalName();
	this.message = message;
	this.lang = lang;
    }

    public String getTimestamp() {
	return timestamp;
    }

    public int getStatus() {
	return status;
    }

    public String getError() {
	return error;
    }

    public String getException() {
	return exception;
    }

    public String getMessage() {
	return message;
    }

    public String getLang() {
	return lang;
    }
}

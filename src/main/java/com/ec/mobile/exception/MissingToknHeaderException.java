package com.ec.mobile.exception;

@SuppressWarnings("serial")
public class MissingToknHeaderException extends RuntimeException {

    private final String message;

    public MissingToknHeaderException(String message) {
	this.message = message;
    }

    public String getMessage() {
	return message;
    }
}

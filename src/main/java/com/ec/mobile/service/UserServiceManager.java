package com.ec.mobile.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.ec.mobile.dao.UserRepository;
import com.ec.mobile.dto.User;

@Service
public class UserServiceManager implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceManager(UserRepository userRepository) {
    	this.userRepository = userRepository;
    }

    public User getUser(String username) {
    	return userRepository.fetchOne(username);
    }
}

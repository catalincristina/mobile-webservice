package com.ec.mobile.service;

import com.ec.mobile.dto.Session;

public interface SessionService {

    Session createSession(String username, String lang);
    Session authorizesSession(String token);
}

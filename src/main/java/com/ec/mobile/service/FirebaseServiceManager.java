package com.ec.mobile.service;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ec.mobile.constant.ErrorConstants;
import com.ec.mobile.dto.Payload;
import com.ec.mobile.exception.PushNotificationException;
import com.ec.mobile.util.Localization;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

@Service
public class FirebaseServiceManager implements FirebaseService {

    private final FirebaseMessaging firebaseMessaging;

    @Autowired
    public FirebaseServiceManager(FirebaseMessaging firebaseMessaging) {
    	this.firebaseMessaging = firebaseMessaging;
    }

    public ResponseEntity<String> send(Payload payload) throws PushNotificationException {
    	String firebaseResponseForIos = null;
    	String firebaseResponseForAndroid = null;

    	try {
    		firebaseResponseForIos = firebaseMessaging.sendAsync(iosMessage(payload)).get();
    		firebaseResponseForAndroid = firebaseMessaging.sendAsync(androidMessage(payload)).get();
    	} catch (InterruptedException | ExecutionException e) {
    		throw new PushNotificationException(HttpStatus.BAD_REQUEST.value(), ErrorConstants.BAD_REQUEST,
						"Eroare la trimiterea notificarii crf-bpo://projects/" + payload.getProjectId()
										  + "/categories/" + payload.getCategoryId()
										  + "/tasks/" + payload.getProcessId()
						+ "catre topic-ul " + payload.getUsername(),
						ErrorConstants.DEFAULT_LANG);
    	}

    	return new ResponseEntity<>(firebaseResponseForIos + " " + firebaseResponseForAndroid, HttpStatus.OK);
    }

    private Message iosMessage(Payload payload) {
    	String condition = "'ios' in topics && '" + payload.getUsername() + "' in topics";

    	Message message = Message.builder()
    						.setApnsConfig(ApnsConfig.builder()
    										.setAps(Aps.builder()
    												.setContentAvailable(true)
    												.build())
    										.build())
    						.setNotification(new Notification("Deconturi", "Aveti alocat un task nou"))
    						.setCondition(condition)
    						.putData("en_title", Localization.parseValue("Deconturi", "en"))
    						.putData("en_message", Localization.parseValue("mesaj", "en"))
    						.putData("fr_title", Localization.parseValue("Deconturi", "fr"))
    						.putData("fr_message", Localization.parseValue("mesaj", "fr"))
    						.putData("ro_title", Localization.parseValue("Deconturi", "ro"))
    						.putData("ro_message", Localization.parseValue("mesaj", "ro"))
    						.putData("action", "crf-bpo://projects/" + payload.getProjectId()
    									+ "/categories/" + payload.getCategoryId()
    									+ "/tasks/" + payload.getProcessId())
    						.build();

    	return message;
    }

    private Message androidMessage(Payload payload) {
    	String condition = "'android' in topics && '" + payload.getUsername() + "' in topics";

    	Message message = Message.builder()
    						.setCondition(condition)
    						.putData("en_title", Localization.parseValue("Deconturi", "en"))
    						.putData("en_message", Localization.parseValue("mesaj", "en"))
    						.putData("fr_title", Localization.parseValue("Deconturi", "fr"))
    						.putData("fr_message", Localization.parseValue("mesaj", "fr"))
    						.putData("ro_title", Localization.parseValue("Deconturi", "ro"))
    						.putData("ro_message", Localization.parseValue("mesaj", "ro"))
    						.putData("action", "crf-bpo://projects/" + payload.getProjectId()
    									+ "/categories/" + payload.getCategoryId()
    									+ "/tasks/" + payload.getProcessId())
    						.build();

    	return message;
    }
}

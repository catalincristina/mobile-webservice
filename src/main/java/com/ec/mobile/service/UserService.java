package com.ec.mobile.service;

import com.ec.mobile.dto.User;

public interface UserService {

    User getUser(String username);
}

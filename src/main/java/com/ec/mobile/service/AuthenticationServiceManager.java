package com.ec.mobile.service;

import org.springframework.stereotype.Service;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import com.ec.mobile.constant.LdapConstants;
import com.ec.mobile.dto.Credential;

@Service
public class AuthenticationServiceManager implements AuthenticationService {

    public boolean doLdapAuthentication(Credential credential) {
    	Hashtable<String, String> env = new Hashtable<String, String>();
    	env.put(Context.INITIAL_CONTEXT_FACTORY, LdapConstants.DEFAULT_INITIAL_CONTEXT_FACTORY);
    	env.put(Context.PROVIDER_URL, LdapConstants.DEFAULT_PROVIDER_URL);
    	env.put(Context.SECURITY_PRINCIPAL, credential.getUsername() + "@" + LdapConstants.DEFAULT_DOMAIN);
    	env.put(Context.SECURITY_CREDENTIALS, credential.getPassword());

    	DirContext ctx = null;

    	try {
    		ctx = new InitialDirContext(env);
    		return true;
    	} catch (NamingException e) {
    		// never mind this
    	} finally {
    		if (ctx != null) {
    			try {
    				ctx.close();
    			} catch (Exception e) {
    				// never mind this
    			}
    		}
    	}

		/*
		 * trebuie sa returneze false
		 * am returnat true deoarece autentificarea cu LDAP nu este activa
		 */
		return true;
	    }
}

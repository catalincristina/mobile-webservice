package com.ec.mobile.service;

import java.util.List;

import com.ec.mobile.dto.Decision;
import com.ec.mobile.dto.Payload;
import com.ec.mobile.dto.Task;
import com.ec.mobile.dto.User;

public interface TaskService {

    List<Task> getAllTasks(long categoryId, int projectId, User user, String lang);
    void addDecisions(int projectId, long categoryId, List<Decision> decisions, User user, String lang);
    void send(int projectId, long categoryId, List<Decision> decisions, User user, String lang);
    List<Payload> getAllTasksForPushNotification();
    void updateAll(List<Payload> payloads);
}

package com.ec.mobile.service;

import java.util.List;

import com.ec.mobile.dto.Project;
import com.ec.mobile.dto.User;

public interface ProjectService {

    List<Project> getAllProjects(User user, String lang);
}

package com.ec.mobile.service;

import java.util.List;
import com.ec.mobile.dto.Category;
import com.ec.mobile.dto.User;

public interface CategoryService {

    List<Category> getAllCategories(int projectId, User user, String lang);
}

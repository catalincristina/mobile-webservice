package com.ec.mobile.service;

// import com.ec.mobile.exception.UserNotFoundException;
// import com.ec.mobile.exception.SessionNotFoundException;
import com.ec.mobile.exception.Mailable;

public interface EmailService {

    // void sendEmail(String to, UserNotFoundException e);
    // void sendEmail(String to, SessionNotFoundException e);
    // void sendEmail(String to, RuntimeException e);
    <T extends Mailable> void sendEmail(String to, T exception);
    void sendEmail(String to, String username, String content);
}

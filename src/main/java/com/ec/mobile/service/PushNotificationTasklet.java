package com.ec.mobile.service;

import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Service;

import com.ec.mobile.constant.EmailConstants;
import com.ec.mobile.dto.Payload;
import com.ec.mobile.exception.MobileDataAccessException;
import com.ec.mobile.exception.PushNotificationException;

@Service
public class PushNotificationTasklet implements Tasklet {
	
	private final TaskService taskService;
	private final FirebaseService firebaseService;
	private final EmailService emailService;
	
	public PushNotificationTasklet(TaskService taskService, FirebaseService firebaseService, EmailService emailService) {
		this.taskService = taskService;
		this.firebaseService = firebaseService;
		this.emailService = emailService;
	}
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		List<Payload> payloads = null;
		
		try {
			payloads = taskService.getAllTasksForPushNotification();
			
			if (payloads.isEmpty()) {
				return RepeatStatus.FINISHED;
			}
			
			taskService.updateAll(payloads);
		}
		catch (MobileDataAccessException e) {
			emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
			return RepeatStatus.FINISHED;
		}
		
		for (Payload payload : payloads) {
			try {
				firebaseService.send(payload);
			}
			catch (PushNotificationException e) {
				emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
			}
		}
		
		/*try {
			for (Payload payload : payloads) {
				if (firebaseService.send(payload).getStatusCode() == HttpStatus.OK) {
					pushList.add(new Push(payload.getProcessId()));
				}
				firebaseService.send(payload);
			}
		} catch (PushNotificationException e) {
			emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
		}*/
		
		return RepeatStatus.FINISHED;
	}
}

package com.ec.mobile.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.ec.mobile.constant.ErrorConstants;
import com.ec.mobile.dao.SessionRepository;
import com.ec.mobile.dto.Session;
import com.ec.mobile.dto.User;
import com.ec.mobile.exception.UserNotFoundException;
import com.ec.mobile.exception.TokenNotCreatedException;
import com.ec.mobile.exception.SessionNotFoundException;
import com.ec.mobile.exception.InvalidSessionException;

@Service
public class SessionServiceManager implements SessionService {

    private final SessionRepository sessionRepository;
    private final UserService userService;

    @Autowired
    public SessionServiceManager(SessionRepository sessionRepository, UserService userService) {
    	this.sessionRepository = sessionRepository;
    	this.userService = userService;
    }

    public Session createSession(String username, String lang) {
    	Session session = sessionRepository.fetchActiveSession(username);

    	if (session == null) {
    		User user = userService.getUser(username);

    		// TODO Dupa activare LDAP, UNAUTHORIZED trebuie inlocui cu INTERNAL_SERVER_ERROR
    		// HttpStatus.INTERNAL_SERVER_ERROE.value() == 500
    		if (user == null) { throw new UserNotFoundException(HttpStatus.NOT_FOUND.value(),
																ErrorConstants.UNAUTHORIZED,
																"Utilizatorul " + username + " nu exista in tabela mobile.app_utilizator",
																lang); }
    		session = new Session(user);
    	} else {
    		sessionRepository.update(session);
    		session = new Session(new User(session.getUserId(), session.getUsername(), session.getEmail()));
    	}
	
    	try {
    		session.setToken(createToken(session));
    	} catch (NullPointerException npe) {
    		throw new TokenNotCreatedException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					       					   ErrorConstants.INTERNAL_SERVER_ERROR,
					       					   "Nu a fost creat token pentru utilizatorul " + username,
					       					   lang);
    	}

    	sessionRepository.save(session);

    	return session;
    }

    public Session authorizesSession(String token) {
    	Session session = sessionRepository.fetchOne(token);

    	if (session == null) { throw new SessionNotFoundException(HttpStatus.NOT_FOUND.value(),
    															  ErrorConstants.FORBIDDEN,
    															  "Token-ul " + token + " nu exista in tabela mobile.web_service",
    															  ErrorConstants.DEFAULT_LANG); }
    	if (!session.isValid()) { throw new InvalidSessionException("Session timeout"); }

    	return session;
    }

    private String createToken(Session session) {
    	String token = session.getUsername() + session.getStartTime();

    	try {
    		MessageDigest md = MessageDigest.getInstance("MD5");
    		md.update(token.getBytes());
    		return toHexString(md.digest());
    	} catch (NoSuchAlgorithmException e) {
    		return null;
    	}
    }

    private String toHexString(byte[] bytes) {
    	StringBuilder sb = new StringBuilder();

    	for (int i = 0; i < bytes.length; i++) {
    		String hex = Integer.toHexString(0xFF & bytes[i]);
    		if (hex.length() == 1) { sb.append('0'); }
    		sb.append(hex);
    	}

    	return sb.toString();
    }
}

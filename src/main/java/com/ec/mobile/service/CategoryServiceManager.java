package com.ec.mobile.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.List;

import com.ec.mobile.constant.ErrorConstants;
import com.ec.mobile.dao.CategoryRepository;
import com.ec.mobile.dto.Category;
import com.ec.mobile.dto.User;
import com.ec.mobile.exception.CategoryNotFoundException;

@Service
public class CategoryServiceManager implements CategoryService {

	private final CategoryRepository categoryRepository;

	@Autowired
	public CategoryServiceManager(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	public List<Category> getAllCategories(int projectId, User user, String lang) {
		List<Category> categories = categoryRepository.fetchAll(projectId, user, lang);

		if (categories.isEmpty()) { throw new CategoryNotFoundException(HttpStatus.NOT_FOUND.value(),
				ErrorConstants.INTERNAL_SERVER_ERROR,
				"CategoryServiceManager.java: 26: Categories not found, projectId = " + projectId
				+ ", username = " + user.getUsername(),
				lang); }	    

		return categories;
	}
}

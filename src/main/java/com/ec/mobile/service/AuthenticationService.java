package com.ec.mobile.service;

import com.ec.mobile.dto.Credential;

public interface AuthenticationService {

    boolean doLdapAuthentication(Credential credential);
}

package com.ec.mobile.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

// import com.ec.mobile.exception.UserNotFoundException;
// import com.ec.mobile.exception.SessionNotFoundException;
import com.ec.mobile.exception.Mailable;

@Service
public class EmailServiceManager implements EmailService {

    private final MailSender mailSender;

    @Autowired
    public EmailServiceManager(MailSender mailSender) {
    	this.mailSender = mailSender;
    }

    public <T extends Mailable> void sendEmail(String to, T e) {
    	SimpleMailMessage message = new SimpleMailMessage();

    	message.setFrom("noreply@enterprise-concept.com");
    	message.setTo(to);
    	message.setSubject("MobileWS :: " + e.getError()); 
    	message.setText(String.format("timestamp: %s\nstatus: %d\nerror: %s\nexception: %s\nmessage: %s\n",
										e.getTimestamp(), e.getStatus(), e.getError(), e.getException(), e.getMessage()));

    	mailSender.send(message);
    }

    public void sendEmail(String to, String username, String content) {
    	SimpleMailMessage message = new SimpleMailMessage();

    	message.setFrom("noreply@enterprise-concept.com");
    	message.setTo(to);
    	message.setSubject("MobileWS :: decizie :: " + username);
    	message.setText(content);

    	mailSender.send(message);
    }
}

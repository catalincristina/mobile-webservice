package com.ec.mobile.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.List;

import com.ec.mobile.constant.ErrorConstants;
import com.ec.mobile.dao.ProjectRepository;
import com.ec.mobile.dto.Project;
import com.ec.mobile.dto.User;
import com.ec.mobile.exception.ProjectNotFoundException;

@Service
public class ProjectServiceManager implements ProjectService {

    public final ProjectRepository projectRepository;

    @Autowired
    public ProjectServiceManager(ProjectRepository projectRepository) {
    	this.projectRepository = projectRepository;
    }

    public List<Project> getAllProjects(User user, String lang) {
    	List<Project> projects = projectRepository.fetchAll(user, lang);

    	if (projects.isEmpty()) {
    		throw new ProjectNotFoundException(HttpStatus.NOT_FOUND.value(), ErrorConstants.INTERNAL_SERVER_ERROR,
    			"1. id-ul utilizatorului " + user.getUsername() + " nu exista in tabela mobile.app_utilizator_proiect;\n"
    					+ "2. utilizatorul " + user.getUsername() + " este inactiv: status = 0 in mobile.app_utilizator_proiect;",
    			lang);
    	}

    	return projects;
    }
}

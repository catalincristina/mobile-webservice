package com.ec.mobile.service;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import com.ec.mobile.constant.EmailConstants;
import com.ec.mobile.constant.ErrorConstants;
import com.ec.mobile.dao.TaskRepository;
import com.ec.mobile.dto.Decision;
import com.ec.mobile.dto.Payload;
import com.ec.mobile.dto.Task;
import com.ec.mobile.dto.User;
import com.ec.mobile.exception.DecisionNotInsertedException;
import com.ec.mobile.exception.DeductionCommunicationException;
import com.ec.mobile.exception.MobileDataAccessException;
import com.ec.mobile.exception.MobileDataAccessTransportException;

@Service
public class TaskServiceManager implements TaskService {

    private final TaskRepository taskRepository;
    private final RestOperations restOperations;

    @Autowired
    public TaskServiceManager(TaskRepository taskRepository, RestOperations restOperations) {
    	this.taskRepository = taskRepository;
    	this.restOperations = restOperations;
    }

    public List<Task> getAllTasks(long categoryId, int projectId, User user, String lang) {
    	
    	List<Task> tasks = taskRepository.fetchAll(categoryId, projectId, user, lang);

    	/*
		if (tasks.isEmpty()) { throw new TaskNotFoundException(HttpStatus.NOT_FOUND.value(),
							       ErrorConstants.INTERNAL_SERVER_ERROR,
							       "TaskServiceManager.java: 31: Task not found, projectId = " + projectId
								    + ", categoryId = " + categoryId
								    + ", username = " + user.getUsername(),
							       lang); }
    	 */

    	return tasks;
    }
    
    public List<Payload> getAllTasksForPushNotification() throws MobileDataAccessException {
    	
    	try {
    		return taskRepository.fetchAllForPushNotification();
    	}
    	catch (MobileDataAccessTransportException e) {
    		throw new MobileDataAccessException.Builder(e.getStatus(), e.getError(), e.getMessage())
    				.setLang(EmailConstants.DEFAULT_LANG)
    				.build();
    	}
    }

    public void addDecisions(int projectId, long categoryId, List<Decision> decisions, User user, String lang) {
    	
    	if (taskRepository.addDecisions(projectId, categoryId, decisions, user) == null) {
    		throw new DecisionNotInsertedException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
    											   ErrorConstants.INTERNAL_SERVER_ERROR,
    											   "Decisions not inserted",
    											   lang);
    	}
    	
    	ping(lang);
    }

    private void ping(String lang) {
    	
    	ResponseEntity<String> response = restOperations.getForEntity(URI.create("http://89.44.122.247:28383/Deconturi/Jobs/WebService/RestCall.do"), String.class);

    	if (response.getStatusCode() != HttpStatus.OK) {
    		throw new DeductionCommunicationException(response.getStatusCodeValue(),
    				ErrorConstants.INTERNAL_SERVER_ERROR,
    				"Eroare la comunicarea dintre MobileWS si DeconturiWS",
    				lang);
    	}
    }
    
    public void updateAll(List<Payload> payloads) throws MobileDataAccessException {
    	
    	try {
    		taskRepository.updateAll(payloads);
    	}
    	catch (MobileDataAccessTransportException e) {
    		throw new MobileDataAccessException.Builder(e.getStatus(), e.getError(), e.getMessage())
    				.setLang(EmailConstants.DEFAULT_LANG)
    				.build();
    	}
    }
    
    public void send(int projectId, long categoryId, List<Decision> decisions, User user, String lang)
    		throws DeductionCommunicationException, MobileDataAccessException {
    	
    	ResponseEntity<String> response = null;
    	
    	for (int i = 0; i < decisions.size(); i++) {
    		Decision decision = decisions.get(i);
    		response = restOperations.getForEntity("http://89.44.122.247:28383/Deconturi/Jobs/WebService/RestCall.do?"
					+ "processOid={processId}&email={userEmail}&rezolutie={response}&idCategorie={categoryId}&comentariu={message}",
					String.class,
					decision.getProcessId(), user.getEmail(), decision.getResponse() ? 1 : 0, categoryId, decision.getResponse() ? "" : decision.getMessage());

    		if (response.getStatusCode() != HttpStatus.OK) {
    			decisions.remove(i);
    			if (decisions.isEmpty()) {
    				throw new DeductionCommunicationException(response.getStatusCodeValue(),
    	    												  ErrorConstants.INTERNAL_SERVER_ERROR,
    	    												  "Eroare la comunicarea dintre MobileWS si DeconturiWS",
    	    												  lang);
    			} else {
    				i--;
    			}
    		}
    	}
    	
    	try {
    		taskRepository.deleteAll(decisions);
    	}
    	catch (MobileDataAccessTransportException e) {
    		throw new MobileDataAccessException.Builder(e.getStatus(), e.getError(), e.getMessage())
    				.setLang(lang)
    				.build();
    	}
    }
}

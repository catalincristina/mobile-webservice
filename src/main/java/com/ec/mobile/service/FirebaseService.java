package com.ec.mobile.service;

import org.springframework.http.ResponseEntity;

import com.ec.mobile.dto.Payload;

public interface FirebaseService {

    ResponseEntity<String> send(Payload payload);
}

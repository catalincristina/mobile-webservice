package com.ec.mobile.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ec.mobile.constant.EmailConstants;
import com.ec.mobile.dto.Decision;
import com.ec.mobile.dto.Error;
import com.ec.mobile.dto.Session;
import com.ec.mobile.dto.Task;
import com.ec.mobile.dto.User;
import com.ec.mobile.exception.BadProcessIdException;
import com.ec.mobile.exception.DecisionNotInsertedException;
import com.ec.mobile.exception.DeductionCommunicationException;
import com.ec.mobile.exception.InvalidSessionException;
import com.ec.mobile.exception.MissingToknHeaderException;
import com.ec.mobile.exception.MobileDataAccessException;
import com.ec.mobile.exception.SessionNotFoundException;
import com.ec.mobile.exception.TaskNotFoundException;
import com.ec.mobile.exception.TaskNotUpdatedException;
import com.ec.mobile.service.EmailService;
import com.ec.mobile.service.SessionService;
import com.ec.mobile.service.TaskService;
import com.ec.mobile.util.Localization;

@RestController
@RequestMapping("/projects/{projectId}/categories/{categoryId}/tasks")
public class TaskController {

    private final SessionService sessionService;
    private final TaskService taskService;
    private final EmailService emailService;

    @Autowired
    public TaskController(SessionService sessionService, TaskService taskService, EmailService emailService) {
    	this.sessionService = sessionService;
    	this.taskService = taskService;
    	this.emailService = emailService;
    }

    @RequestMapping(method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<Map<String, List<Task>>> tasks(@PathVariable int projectId,
							 							 @PathVariable long categoryId,
							 							 @RequestHeader(name="X-CRF-Session", required=false) String token,
							 							 @RequestHeader(name="X-CRF-Lang", required=false) String lang) {
    	
    	if (token == null || lang == null) { throw new MissingToknHeaderException("Missing header in request"); }

    	Session session = sessionService.authorizesSession(token);

    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

    	Map<String, List<Task>> model = new HashMap<>();
    	model.put("items", taskService.getAllTasks(categoryId, projectId, new User(session.getUserId(), session.getUsername(), session.getEmail()), lang));

    	return new ResponseEntity<Map<String, List<Task>>>(model, responseHeaders, HttpStatus.OK);
    }

    @RequestMapping(value="/decisions", method=RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<String> updateTasks(@PathVariable int projectId,
					     					  @PathVariable long categoryId,
					     					  @RequestBody List<Decision> decisions,
					     					  @RequestHeader(name="X-CRF-Session", required=false) String token,
					     					  @RequestHeader(name="X-CRF-Lang", required=false) String lang) {
    	
    	if (token == null || lang == null) { throw new MissingToknHeaderException("Missing header in request"); }
    	// if (taskId != decision.getProcessId()) { throw new BadProcessIdException("Wrong processId"); }

    	Session session = sessionService.authorizesSession(token);

    	// taskService.addDecisions(projectId, categoryId, decisions, new User(session.getUserId(), session.getUsername(), session.getEmail()), lang);
    	taskService.send(projectId, categoryId, decisions, new User(session.getUserId(), session.getUsername(), session.getEmail()), lang);

    	return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler(MissingToknHeaderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error missingToknHeader(MissingToknHeaderException e) {
    	return new Error(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(BadProcessIdException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error badProcessId(BadProcessIdException e) {
    	return new Error(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(SessionNotFoundException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Error sessionNotFound(SessionNotFoundException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.FORBIDDEN.value(), e.getError());
    }

    @ExceptionHandler(InvalidSessionException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Error invalidSession(InvalidSessionException e) {
    	return new Error(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
    }

    @ExceptionHandler(TaskNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error taskNotFound(TaskNotFoundException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), Localization.parseError(e.getError(), e.getLang()));
    }

    @ExceptionHandler(DeductionCommunicationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error deductionCommunication(DeductionCommunicationException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), Localization.parseError(e.getError(), e.getLang()));
    }
    
    @ExceptionHandler(MobileDataAccessException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error mobileDataAccess(MobileDataAccessException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), Localization.parseError(e.getError(), e.getLang()));
    }

    @ExceptionHandler(TaskNotUpdatedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error taskNotUpdated(TaskNotUpdatedException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getError());
    }

    @ExceptionHandler(DecisionNotInsertedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error decisionNotInserted(DecisionNotInsertedException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getError());
    }
}

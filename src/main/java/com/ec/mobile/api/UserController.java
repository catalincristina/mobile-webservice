package com.ec.mobile.api;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Map;
import java.util.HashMap;

import com.ec.mobile.constant.ErrorConstants;
import com.ec.mobile.constant.EmailConstants;
import com.ec.mobile.dto.Credential;
import com.ec.mobile.dto.Error;
import com.ec.mobile.dto.Session;
import com.ec.mobile.exception.CredentialNotFoundException;
import com.ec.mobile.exception.UserNotFoundException;
import com.ec.mobile.exception.MissingLangHeaderException;
import com.ec.mobile.exception.TokenNotCreatedException;
import com.ec.mobile.service.AuthenticationService;
import com.ec.mobile.service.SessionService;
import com.ec.mobile.service.EmailService;
import com.ec.mobile.util.Localization;

@RestController
@RequestMapping("/login")
public class UserController {
    
    private final AuthenticationService authenticationService;
    private final SessionService sessionService;
    private final EmailService emailService;

    @Autowired
    public UserController(AuthenticationService authenticationService, SessionService sessionService, EmailService emailService) {
    	this.authenticationService = authenticationService;
    	this.sessionService = sessionService;
    	this.emailService = emailService;
    }

    @RequestMapping(method=RequestMethod.POST, consumes="application/json")
    public ResponseEntity<Map<String, Object>> login(@RequestBody Credential credential,
    												 @RequestHeader(name="X-CRF-Lang", required=false) String lang) {
    	if (lang == null) { throw new MissingLangHeaderException("Missing Lang-Header in request"); }
    	if (!authenticationService.doLdapAuthentication(credential)) { throw new CredentialNotFoundException(ErrorConstants.UNAUTHORIZED, lang); }

    	Session session = sessionService.createSession(credential.getUsername(), lang);

    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

    	Map<String, Object> model = new HashMap<>();
    	model.put("sessionId", session.getToken());
    	model.put("topics", new String[] { credential.getUsername(), "users" });

    	return new ResponseEntity<Map<String, Object>>(model, responseHeaders, HttpStatus.OK);
    }

    @ExceptionHandler(MissingLangHeaderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error missingLangHeader(MissingLangHeaderException e) {
    	return new Error(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(CredentialNotFoundException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Error credentialNotFound(CredentialNotFoundException e) {
    	return new Error(HttpStatus.UNAUTHORIZED.value(), Localization.parseError(e.getError(), e.getLang()));
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Error userNotFound(UserNotFoundException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.UNAUTHORIZED.value(), Localization.parseError(e.getError(), e.getLang()));
    }

    @ExceptionHandler(TokenNotCreatedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error tokenNotCreated(TokenNotCreatedException e) {
    	return new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), Localization.parseError(e.getError(), e.getLang()));
    }
}

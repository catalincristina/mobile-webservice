package com.ec.mobile.api;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.ec.mobile.constant.EmailConstants;
import com.ec.mobile.dto.Error;
import com.ec.mobile.dto.Category;
import com.ec.mobile.dto.Session;
import com.ec.mobile.dto.User;
import com.ec.mobile.exception.MissingToknHeaderException;
import com.ec.mobile.exception.SessionNotFoundException;
import com.ec.mobile.exception.InvalidSessionException;
import com.ec.mobile.exception.CategoryNotFoundException;
import com.ec.mobile.service.SessionService;
import com.ec.mobile.service.CategoryService;
import com.ec.mobile.service.EmailService;
import com.ec.mobile.util.Localization;

@RestController
@RequestMapping("/projects/{projectId}/categories")
public class CategoryController {

    private final SessionService sessionService;
    private final CategoryService categoryService;
    private final EmailService emailService;

    @Autowired
    public CategoryController(SessionService sessionService, CategoryService categoryService, EmailService emailService) {
    	this.sessionService = sessionService;
    	this.categoryService = categoryService;
    	this.emailService = emailService;
    }

    @RequestMapping(method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<Map<String, List<Category>>> categories(@PathVariable int projectId,
								  								  @RequestHeader(name="X-CRF-Session", required=false) String token,
								  								  @RequestHeader(name="X-CRF-Lang", required=false) String lang) {
    	if (token == null || lang == null) { throw new MissingToknHeaderException("Missing header in request"); }

    	Session session = sessionService.authorizesSession(token);

    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

    	Map<String, List<Category>> model = new HashMap<>();
    	model.put("categories", categoryService.getAllCategories(projectId, new User(session.getUserId(), session.getUsername(), session.getEmail()), lang));

    	return new ResponseEntity<Map<String, List<Category>>>(model, responseHeaders, HttpStatus.OK);
    }

    @ExceptionHandler(MissingToknHeaderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error missingToknHeader(MissingToknHeaderException e) {
    	return new Error(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(SessionNotFoundException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Error sessionNotFound(SessionNotFoundException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.FORBIDDEN.value(), e.getError());
    }

    @ExceptionHandler(InvalidSessionException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Error invalidSession(InvalidSessionException e) {
    	return new Error(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error categoryNotFound(CategoryNotFoundException e) {
    	emailService.sendEmail(EmailConstants.CATALIN_CRISTINA, e);
    	return new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), Localization.parseError(e.getError(), e.getLang()));
    }
}

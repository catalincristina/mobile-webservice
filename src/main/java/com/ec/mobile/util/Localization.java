package com.ec.mobile.util;

import java.util.ResourceBundle;
import java.util.Locale;

public class Localization {

    public static String parseValue(String value, String lang) {
	switch (lang) {
	    case "ro":
		return ResourceBundle.getBundle("deconturi", new Locale(lang)).getString(value);
	    case "fr":
		return ResourceBundle.getBundle("deconturi", Locale.FRENCH).getString(value);
	    // "en" is default language
	    default:
		return ResourceBundle.getBundle("deconturi", Locale.ENGLISH).getString(value);
	}
    }

    public static String parseError(String error, String lang) {
	switch (lang) {
	    case "ro":
		return ResourceBundle.getBundle("erori", new Locale(lang)).getString(error);
	    case "fr":
		return ResourceBundle.getBundle("erori", Locale.FRENCH).getString(error);
	    // "en" is default language
	    default:
		return ResourceBundle.getBundle("erori", Locale.ENGLISH).getString(error);
	}
    }
}

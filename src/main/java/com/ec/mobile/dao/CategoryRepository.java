package com.ec.mobile.dao;

import java.util.List;
import com.ec.mobile.dto.Category;
import com.ec.mobile.dto.User;

public interface CategoryRepository {

    List<Category> fetchAll(int projectId, User user, String lang);
}

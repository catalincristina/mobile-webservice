package com.ec.mobile.dao;

import java.util.List;

import com.ec.mobile.dto.Decision;
import com.ec.mobile.dto.Payload;
import com.ec.mobile.dto.Task;
import com.ec.mobile.dto.User;

public interface TaskRepository {

    List<Task> fetchAll(long categoryId, int projectId, User user, String lang);
    int[] addDecisions(int projectId, long categoryId, List<Decision> decisions, User user);
    List<Payload> fetchAllForPushNotification();
    int[] updateAll(List<Payload> payloads);
    int[] deleteAll(List<Decision> decisions);
}

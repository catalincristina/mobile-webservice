package com.ec.mobile.dao;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ec.mobile.dto.Project;
import com.ec.mobile.dto.User;
import com.ec.mobile.util.Localization;

@Repository
public class ProjectRepositoryManager implements ProjectRepository {

    private static final String SELECT_ALL_PROJECTS_BY_USER_ID = "SELECT p.id, p.denumire, p.urlImagine\n"
    		+ "FROM app_utilizator_proiect up\n"
    		+ "INNER JOIN app_utilizator u ON u.id = up.idUtilizator\n"
    		+ "INNER JOIN app_proiect p ON p.id = up.idProiect\n"
    		+ "WHERE up.idUtilizator = ? AND p.status = 1";

    private static final String SELECT_COUNT_TASKS_BY_EMAIL = "SELECT COUNT(idProces) AS itemCount\n"
    		+ "FROM vwmobiledataexport\n"
    		+ "WHERE email = ?";

    private final JdbcOperations jdbcOperations;

    @Autowired
    public ProjectRepositoryManager(JdbcOperations jdbcOperations) {
    	this.jdbcOperations = jdbcOperations;
    }

    public List<Project> fetchAll(final User user, final String lang) {
    	return jdbcOperations.query(
    			SELECT_ALL_PROJECTS_BY_USER_ID,
    			new RowMapper<Project>() {
    				public Project mapRow(ResultSet rs, int rowNum) throws SQLException {
    					return new Project(rs.getInt(1), Localization.parseValue(rs.getString(2), lang), rs.getString(3), getItemCount(user.getEmail()));
    				}
    			},
    			user.getId());
    }

    private int getItemCount(String email) {
    	try {
    		return jdbcOperations.queryForObject(
    				SELECT_COUNT_TASKS_BY_EMAIL,
    				new RowMapper<Integer>() {
    					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
    						return new Integer(rs.getInt(1));
    					}
    				},
    				email);
    	} catch (EmptyResultDataAccessException e) {
    		return 0;
    	}
    }
}

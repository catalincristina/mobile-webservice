package com.ec.mobile.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ec.mobile.constant.ErrorConstants;
import com.ec.mobile.dto.Decision;
import com.ec.mobile.dto.Field;
import com.ec.mobile.dto.Payload;
import com.ec.mobile.dto.Record;
import com.ec.mobile.dto.Section;
import com.ec.mobile.dto.Task;
import com.ec.mobile.dto.Transport;
import com.ec.mobile.dto.User;
import com.ec.mobile.exception.MobileDataAccessTransportException;
import com.ec.mobile.util.Localization;

@Repository
public class TaskRepositoryManager implements TaskRepository {

	private static final String SELECT_ALL_RECORDS_BY_CATEGORY_ID_AND_USER_MAIL = "SELECT id, numeTask, idProces, idReferinta, tabelaReferinta, "
			+ "Data, Magazin, NumeBeneficiar, avans, decont, cheltuiala, moneda\n"
			+ "FROM vwmobiledataexport\n"
			+ "WHERE idCategorie = ? AND email = ?";

	private static final String INSERT_DECISION = "INSERT INTO WebServiceLog (processOid, email, rezolutie, idCategorie, comentariu)\n"
			+ "VALUES (?, ?, ?, ?, ?)";
	
	private static final String SELECT_ALL_FOR_PUSH_NOTIFICATION = "SELECT id, idProces, idCategorie, email, push\n"
			+ "FROM vwmobiledataexport\n"
			+ "WHERE push = 0";
	
	private static final String UPDATE_ALL = "UPDATE vwmobiledataexport\n"
			+ "SET push = 1\n"
			+ "WHERE id = ?";
	
	private static final String DELETE_ALL = "DELETE FROM vwmobiledataexport\n"
			+ "WHERE id = ?";

	
	private final JdbcOperations jdbcOperations;

	@Autowired
	public TaskRepositoryManager(JdbcOperations jdbcOperations) {
		this.jdbcOperations = jdbcOperations;
	}

    public int[] updateAll(final List<Payload> payloads) throws MobileDataAccessTransportException {
    	
    	try {
    		return jdbcOperations.batchUpdate(
    				UPDATE_ALL,
    				new BatchPreparedStatementSetter() {
    		    		public void setValues(PreparedStatement ps, int i) throws SQLException {
    						ps.setLong(1, payloads.get(i).getId());
    		    		}

    		    		public int getBatchSize() {
    						return payloads.size();
    		    		}
    				});
    	}
    	catch (DataAccessException e) {
    		throw new MobileDataAccessTransportException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
    				ErrorConstants.INTERNAL_SERVER_ERROR,
    				this.getClass().getCanonicalName()
    				+ " :: " + new Object() {}.getClass().getEnclosingMethod().getName());
    	}
		
    }
    
    public int[] deleteAll(final List<Decision> decisions) throws MobileDataAccessTransportException {
    	
    	try {
    		return jdbcOperations.batchUpdate(
    				DELETE_ALL,
    				new BatchPreparedStatementSetter() {
    		    		public void setValues(PreparedStatement ps, int i) throws SQLException {
    						ps.setLong(1, decisions.get(i).getId());
    		    		}

    		    		public int getBatchSize() {
    						return decisions.size();
    		    		}
    				});
    	}
    	catch (DataAccessException e) {
    		throw new MobileDataAccessTransportException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					 ErrorConstants.INTERNAL_SERVER_ERROR,
					 this.getClass().getCanonicalName()
					 + " :: " + new Object() {}.getClass().getEnclosingMethod().getName());
    	}
		
    }

    public int[] addDecisions(int projectId, final long categoryId, final List<Decision> decisions, final User user) {
    	
    	try {
    		return jdbcOperations.batchUpdate(
    				INSERT_DECISION,
    				new BatchPreparedStatementSetter() {
    					public void setValues(PreparedStatement ps, int i) throws SQLException {
    						Decision decision = decisions.get(i);
    						ps.setLong(1, decision.getProcessId());
    						ps.setString(2, user.getEmail());
    						ps.setBoolean(3, decision.getResponse());
    						ps.setLong(4, categoryId);
    						ps.setString(5, decision.getMessage());
    					}

    					public int getBatchSize() {
    						return decisions.size();
    					}
    				});
    	}
    	catch (DataAccessException e) {
    		throw new MobileDataAccessTransportException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					 ErrorConstants.INTERNAL_SERVER_ERROR,
					 this.getClass().getCanonicalName()
					 + " :: " + new Object() {}.getClass().getEnclosingMethod().getName());
    	}
    }
    
    public List<Payload> fetchAllForPushNotification() throws MobileDataAccessTransportException {
    	
    	try {
    		return jdbcOperations.query(SELECT_ALL_FOR_PUSH_NOTIFICATION,
        			new RowMapper<Payload>() {
        				public Payload mapRow(ResultSet rs, int rowNum) throws SQLException {
        					return new Payload(rs.getLong(1), rs.getLong(2), rs.getLong(3), rs.getString(4), rs.getBoolean(5));
        				}
        			});
    	}
    	catch (DataAccessException e) {
    		throw new MobileDataAccessTransportException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
    				ErrorConstants.INTERNAL_SERVER_ERROR,
    				this.getClass().getCanonicalName()
    				+ " :: " + new Object() {}.getClass().getEnclosingMethod().getName());
    	}
    	
    }

    public List<Task> fetchAll(long categoryId, int projectId, User user, String lang) {
    	
    	List<Record> records = fetchAllRecords(categoryId, projectId, user, lang);

    	if (records.isEmpty()) {
    		return new ArrayList<Task>();
    	}

    	List<Task> tasks = new ArrayList<>(records.size());

    	for (Record record : records) {
    		tasks.add(new Task.Builder(record.getTaskName(), new Transport(record.getId(), record.getProcessId(), record.getReferenceId(), record.getReferenceTable()))
    				.fields(buildAllPrimaryFields(record, lang))
    				.details(buildAllSections(record, lang))
    				.build());
    	}

    	return tasks;
    }

    private List<Record> fetchAllRecords(long categoryId, int projectId, User user, final String lang) {
    	
    	return jdbcOperations.query(
    			SELECT_ALL_RECORDS_BY_CATEGORY_ID_AND_USER_MAIL,
    			new RowMapper<Record>() {
    				public Record mapRow(ResultSet rs, int rowNum) throws SQLException {
    					return new Record(rs.getLong(1), Localization.parseValue(rs.getString(2), lang), rs.getLong(3), rs.getLong(4), rs.getString(5), rs.getDate(6),
    							rs.getString(7), rs.getString(8), rs.getDouble(9), rs.getDouble(10), rs.getString(11), rs.getString(12));
    				}
    			},
    			categoryId, user.getEmail());
    }

    private List<Section> buildAllSections(Record record, String lang) {
    	
    	List<Section> sections = new ArrayList<>();

    	sections.add(new Section.Builder(record.getTaskName())
    			.fields(buildAllSecondaryHeaderFields(record, lang))
    			.build());
    	sections.add(new Section.Builder(Localization.parseValue("Detalii", lang))
    			.fields(buildAllSecondaryBodyFields(record, lang))
    			.build());

    	return sections;
    }

    private List<Field> buildAllPrimaryFields(Record record, String lang) {
    	
    	List<Field> fields = new ArrayList<>();

    	fields.add(new Field(Localization.parseValue("Data", lang), record.getRequestDate().toString()));
    	fields.add(new Field(Localization.parseValue("Beneficiar", lang), record.getRequesterName()));
    	fields.add(new Field(Localization.parseValue("Magazin", lang), record.getStore()));
    	fields.add(new Field(Localization.parseValue("Avans", lang), String.format("%.2f %s", record.getAdvance(), record.getCurrency())));
    	fields.add(new Field(Localization.parseValue("Decont", lang), String.format("%.2f %s", record.getDeduction(), record.getCurrency())));
    	// fields.add(new Field(Localization.parseValue("Categorie", lang), Localization.parseValue(record.getCategory(), lang)));
    	fields.add(new Field(Localization.parseValue("Cheltuiala", lang), record.getExpense()));

    	return fields;
    }

    private List<Field> buildAllSecondaryHeaderFields(Record record, String lang) {
    	
    	List<Field> fields = new ArrayList<>();

    	fields.add(new Field(Localization.parseValue("Data", lang), record.getRequestDate().toString()));
    	fields.add(new Field(Localization.parseValue("Beneficiar", lang), record.getRequesterName()));
    	fields.add(new Field(Localization.parseValue("Magazin", lang), record.getStore()));
    	fields.add(new Field(Localization.parseValue("Avans", lang), String.format("%.2f %s", record.getAdvance(), record.getCurrency())));
    	fields.add(new Field(Localization.parseValue("Decont", lang), String.format("%.2f %s", record.getDeduction(), record.getCurrency())));

    	return fields;
    }

    private List<Field> buildAllSecondaryBodyFields(Record record, String lang) {
    	
    	List<Field> fields = new ArrayList<>();

    	fields.add(new Field(Localization.parseValue("Avans", lang), String.format("%.2f %s", record.getAdvance(), record.getCurrency())));
    	fields.add(new Field(Localization.parseValue("Decont", lang), String.format("%.2f %s", record.getDeduction(), record.getCurrency())));
    	fields.add(new Field(Localization.parseValue("Diferenta", lang), String.format("%.2f %s", record.getAdvance() - record.getDeduction(), record.getCurrency())));

    	return fields;
    }
}

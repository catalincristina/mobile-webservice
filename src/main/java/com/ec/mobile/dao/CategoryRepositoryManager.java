package com.ec.mobile.dao;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ec.mobile.dto.Category;
import com.ec.mobile.dto.User;
import com.ec.mobile.util.Localization;

@Repository
public class CategoryRepositoryManager implements CategoryRepository {

	private static final String SELECT_ALL_CATEGORIES_BY_PROJECT_ID = "SELECT idCategorie, categorie, COUNT(idProces) AS count\n"
			+ "FROM vwmobiledataexport\n"
			+ "WHERE email = ?\n"
			+ "GROUP BY idCategorie, categorie";

	private final JdbcOperations jdbcOperations;

	@Autowired
	public CategoryRepositoryManager(JdbcOperations jdbcOperations) {
		this.jdbcOperations = jdbcOperations;
	}

	public List<Category> fetchAll(final int projectId, User user, final String lang) {
		return jdbcOperations.query(
				SELECT_ALL_CATEGORIES_BY_PROJECT_ID,
				new RowMapper<Category>() {
					public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Category(rs.getLong(1), projectId, Localization.parseValue(rs.getString(2), lang), rs.getInt(3));
					}
				},
				user.getEmail());
	}
}

package com.ec.mobile.dao;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.ec.mobile.dto.User;

@Repository
public class UserRepositoryManager implements UserRepository {

    private static final String SELECT_USER_BY_USERNAME = "SELECT id, loginName, email FROM app_utilizator WHERE loginName = ?";
    private static final String SELECT_USER_BY_ID	= "SELECT id, loginName, email FROM app_utilizator WHERE id = ?";

    @Autowired
    @Qualifier("mobileJdbcOperations")
    private JdbcOperations jdbcOperations;

    /*
     *
     *
    @Autowired
    @Qualifier("mobileJdbcOperations")
    public UserRepositoryManager(JdbcOperations jdbcOperations) {
	this.jdbcOperations = jdbcOperations;
    }
    *
    */

    public User fetchOne(String username) {
	try {
	    return jdbcOperations.queryForObject(
		SELECT_USER_BY_USERNAME,
		new RowMapper<User>() {
		    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new User(rs.getLong(1), rs.getString(2), rs.getString(3));
		    }
		},
		username);
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }

    public User fetchOne(long userId) {
	try {
	    return jdbcOperations.queryForObject(
		SELECT_USER_BY_ID,
		new RowMapper<User>() {
		    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new User(rs.getLong(1), rs.getString(2), rs.getString(3));
		    }
		},
		userId);
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }
}

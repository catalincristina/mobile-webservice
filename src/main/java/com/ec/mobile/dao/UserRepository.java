package com.ec.mobile.dao;

import com.ec.mobile.dto.User;

public interface UserRepository {

    User fetchOne(String username);
    User fetchOne(long userId);
}

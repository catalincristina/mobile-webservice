package com.ec.mobile.dao;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.dao.EmptyResultDataAccessException;
// import org.springframework.jdbc.core.PreparedStatementCreator;
// import org.springframework.jdbc.support.KeyHolder;
// import org.springframework.jdbc.support.GeneratedKeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
// import java.sql.PreparedStatement;
// import java.sql.Connection;
// import java.sql.Statement;

import com.ec.mobile.dto.Session;
import com.ec.mobile.constant.SessionConstants;

@Repository
public class SessionRepositoryManager implements SessionRepository {

	private static final String SELECT_ACTIVE_SESSION_BY_USERNAME = "SELECT ws.*, u.email\n"
			+ "FROM " + SessionConstants.SESSION_TABLE + " ws\n"
			+ "INNER JOIN app_utilizator u ON u.id = ws.idUser\n"
			+ "WHERE username = ? AND status = 1";

	private static final String SELECT_SESSION_BY_TOKEN = "SELECT ws.*, u.email\n"
			+ "FROM " + SessionConstants.SESSION_TABLE + " ws\n"
			+ "INNER JOIN app_utilizator u ON u.id = ws.idUser\n"
			+ "WHERE sessionToken = ?";

	private static final String INSERT_SESSION = "INSERT INTO " + SessionConstants.SESSION_TABLE + " (idUser, username, sessionToken, startTime, endTime, status)\n"
			+ "VALUES (?, ?, ?, ?, ?, ?)";

	private static final String UPDATE_ACTIVE_SESSION_BY_SESSION_ID = "UPDATE " + SessionConstants.SESSION_TABLE + "\n"
			+ "SET status = 0\n"
			+ "WHERE id = ?";

	@Autowired
	@Qualifier("mobileJdbcOperations")
	private JdbcOperations jdbcOperations;

	/*
	 *
	 * 
    @Autowired
    @Qualifier("mobileJdbcOperations")
    public SessionRepositoryManager(JdbcOperations jdbcOperations) {
	this.jdbcOperations = jdbcOperations;
    }
	 *
	 */

	public Session fetchActiveSession(String username) {
		try {
			return jdbcOperations.queryForObject(
					SELECT_ACTIVE_SESSION_BY_USERNAME,
					new RowMapper<Session>() {
						public Session mapRow(ResultSet rs, int rowNum) throws SQLException {
							return new Session(rs.getLong(1), rs.getLong(2), rs.getString(3), rs.getString(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getBoolean(7), rs.getString(8));
						}
					},
					username);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public Session fetchOne(String token) {
		try {
			return jdbcOperations.queryForObject(
					SELECT_SESSION_BY_TOKEN,
					new RowMapper<Session>() {
						public Session mapRow(ResultSet rs, int rowNum) throws SQLException {
							return new Session(rs.getLong(1), rs.getLong(2), rs.getString(3), rs.getString(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getBoolean(7), rs.getString(8));
						}
					},
					token);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public void save(Session session) {
		jdbcOperations.update(
				INSERT_SESSION,
				session.getUserId(),
				session.getUsername(),
				session.getToken(),
				session.getStartTime(),
				session.getEndTime(),
				session.getStatus());
	}

	public void update(Session session) {
		jdbcOperations.update(
				UPDATE_ACTIVE_SESSION_BY_SESSION_ID,
				session.getId());
	}

	/*
	 * este OK pentru cache
	 *
    public Session save(Session session) {
	KeyHolder keyHolder = new GeneratedKeyHolder();

	jdbc.update(
		new PreparedStatementCreator() {
		    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement ps = con.prepareStatement(INSERT_SESSION, Statement.RETURN_GENERATED_KEYS);
			ps.setLong(1, session.getUserId());
			ps.setString(2, session.getUsername());
			ps.setString(3, session.getToken());
			ps.setTimestamp(4, session.getStartTime());
			ps.setTimestamp(5, session.getEndTime());
			ps.setBoolean(6, session.getStatus());

			return ps;
		    }
		},
		keyHolder);

	if (keyHolder.getKey() == null) {
	    throw new NullPointerException("Write error");
	}

	session.setId(keyHolder.getKey().longValue());

	return session;
    }
	 */
}

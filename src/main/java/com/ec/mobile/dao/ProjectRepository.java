package com.ec.mobile.dao;

import java.util.List;
import com.ec.mobile.dto.Project;
import com.ec.mobile.dto.User;

public interface ProjectRepository {

    List<Project> fetchAll(User user, String lang);
}

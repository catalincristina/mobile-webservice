package com.ec.mobile.dao;

import com.ec.mobile.dto.Session;

public interface SessionRepository {

    Session fetchActiveSession(String username);
    Session fetchOne(String token);
    void save(Session session);
    void update(Session session);
}

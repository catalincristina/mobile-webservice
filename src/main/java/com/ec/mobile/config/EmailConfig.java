package com.ec.mobile.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class EmailConfig {

    @Bean
    public MailSender mailSender(Environment env) {
    	JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    	mailSender.setHost(env.getProperty("spring.mail.host"));
    	mailSender.setPort(Integer.parseInt(env.getProperty("spring.mail.port")));
    	mailSender.setUsername(env.getProperty("spring.mail.username"));
    	mailSender.setPassword(env.getProperty("spring.mail.password"));

    	Properties props = mailSender.getJavaMailProperties();
    	props.setProperty("mail.transport.protocol", "smtp");
    	props.setProperty("mail.smtp.auth", "true");
    	props.setProperty("mail.smtp.starttls.enable", "true");

    	mailSender.setJavaMailProperties(props);

    	return mailSender;
    }
}

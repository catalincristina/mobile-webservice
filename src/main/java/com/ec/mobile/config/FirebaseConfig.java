package com.ec.mobile.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.Resource;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.auth.oauth2.GoogleCredentials;

import java.io.IOException;

@Configuration
public class FirebaseConfig {

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public FirebaseApp firebaseApp() {
    	Resource resource = resourceLoader.getResource("classpath:carf-bpo-firebase.json");
    	FirebaseOptions firebaseOptions = null;

    	try {
    		firebaseOptions = new FirebaseOptions.Builder()
										.setCredentials(GoogleCredentials.fromStream(resource.getInputStream()))
										.setDatabaseUrl("https://carf-bpo.firebaseio.com")
										.setProjectId("carf-bpo")
										.build();
    		return FirebaseApp.initializeApp(firebaseOptions);
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	return null;
    }

    @Bean
    public FirebaseMessaging firebaseMessaging(FirebaseApp firebaseApp) {
    	return FirebaseMessaging.getInstance(firebaseApp);
    }
}

package com.ec.mobile.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration
public class DataSourceConfig {

    @Bean
    public DataSource mobileDataSource() {
    	BasicDataSource basicDataSource = new BasicDataSource();

    	basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
    	basicDataSource.setUrl("jdbc:mysql://89.44.122.235:3306/mobile?useSSL=true&verifyServerCertificate=true");
    	basicDataSource.setUsername("root");
    	basicDataSource.setPassword("Concept2015!");
    	basicDataSource.setInitialSize(5);
    	basicDataSource.setMaxTotal(10);

    	// production
    	/*
		basicDataSource.setUrl("jdbc:mysql://10.191.37.83:3306/mobile?useSSL=true&verifyServerCertificate=true");
		basicDataSource.setUsername("root");
		basicDataSource.setPassword("dbC0n!65");
		basicDataSource.setInitialSize(5);
		basicDataSource.setMaxTotal(10);
    	 */

    	return basicDataSource;
    }


    @Bean
    public JdbcOperations mobileJdbcOperations() {
    	return new JdbcTemplate(mobileDataSource());
    }

    @Bean
    public NamedParameterJdbcOperations mobileNamedParameterJdbcOperations() {
    	return new NamedParameterJdbcTemplate(mobileDataSource());
    }
}
